import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatMenuModule } from '@angular/material/menu';
import { HomePageComponent } from './components/home-page/home-page.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { MenuComponent } from './components/menu/menu.component';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { TeamComponent } from './components/team/team.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { CvComponent } from './components/cv/cv.component';
import { FeedbackComponent } from './components/feedback/feedback.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { MatFormFieldModule, MatFormFieldControl } from '@angular/material/form-field'
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from "@angular/forms"


@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    HeaderComponent,
    FooterComponent,
    MenuComponent,
    TeamComponent,
    CvComponent,
    FeedbackComponent,
    AboutUsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatButtonModule,
    MatSidenavModule,
    MatMenuModule,
    MatCardModule,
    MatGridListModule,
    MatFormFieldModule,
    PdfViewerModule,
    FormsModule,

    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: HomePageComponent },
      { path: 'Menu', component: MenuComponent },
      { path: 'Team', component: TeamComponent },
      { path: 'Feedback', component: FeedbackComponent },
      { path: 'AboutUs', component: AboutUsComponent }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
