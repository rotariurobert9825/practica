import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {
  foo: string[] = [];
  comment;

  constructor() { }

  ngOnInit() {

  }

  ShowComment() {
    this.foo.push(this.comment);
    console.log(this.foo[0]);
    this.comment = '';
  }
}
